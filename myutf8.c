#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LIBUTF8_IMPLEMENTATION
#include "libutf8.h"

void test_utf8_encode_located(char *file, int line, unsigned char *buf,
                              uint32_t c, int expected) {
  int actual = utf8_encode(buf, c);
  if (actual == expected) {
    printf("encode %x in utf8 => %s\n", c, buf);
  } else {
    printf("%s:%d: FAILURE! expected=%d, but actual=%d\n", file, line, expected,
           actual);
  }
}

#define test_utf8_encode(c, expected)                               \
  do {                                                              \
    static unsigned char buf[4] = {0};                              \
    test_utf8_encode_located(__FILE__, __LINE__, buf, c, expected); \
  } while (0)

void test_utf8_decode_located(char *file, int line, char *utf8) {
  char *newp[1];
  uint32_t codepoint = utf8_decode(utf8, newp);
  printf("codepoint: %x\n", codepoint);
  unsigned char buf[4] = {0};
  int len = utf8_encode(buf, codepoint);
  /* buf[len]='\0'; */
  test_utf8_encode(codepoint, len);
  /* assert(len == *newp - utf8); */
  assert(0 == memcmp(buf, utf8, len));
  printf("%s == %s, codepoint=%d\n\n", buf, utf8, codepoint);

  for (size_t i=0; i<=len;i++){
    buf[i]=0;
  }
}

#define test_utf8_decode(utf8) \
  test_utf8_decode_located(__FILE__, __LINE__, utf8)

int main(void) {
  test_utf8_encode(0x7E, 1);
  test_utf8_encode(0x7F, 1);
  test_utf8_encode(0x80, 2);
  test_utf8_encode(0xC0, 2);
  test_utf8_encode(0xE0, 2);
  test_utf8_encode(0x700, 2);
  test_utf8_encode(0x7FF, 2);
  test_utf8_encode(22900, 3);
  test_utf8_encode(0x20AC, 3);
  test_utf8_encode(0xFFFF, 3);
  test_utf8_encode(0x4E01, 3);
  test_utf8_encode(0x10FFFF, 4);

  test_utf8_decode("丁");
  test_utf8_decode("好");
  test_utf8_decode("世");
  test_utf8_decode("界");

#if 0
  /* 打印 unicode 中所有汉字 */
  unsigned char buf[4] = {0};
  for (uint32_t i = 0x4E00; i < 0x9FFF; i++) {
    int len = utf8_encode(buf, i);
    printf("%x=%d=%s\n", i, i, buf);
    for (int j = 0; j < len; j++) buf[j] = 0;
  }
#endif

  return 0;
}
