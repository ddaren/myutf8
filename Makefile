CC=zig cc
CFLAGS=-std=c99 -g -Wall
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)

myutf8:$(OBJS)
	$(CC) $(CFLAGS) -o $@ $?

run:
	./myutf8

clean:
	rm *.o myutf8
