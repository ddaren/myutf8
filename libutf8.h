#ifndef LIBUTF8_H__
#define LIBUTF8_H__
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int utf8_encode(unsigned char *buf, uint32_t c);
uint32_t utf8_decode(char *p, char **newp);
bool utf8_is_legal(char *p);
int utf8_length(char *p);

#endif

#ifdef LIBUTF8_IMPLEMENTATION

int swap_endians(int value)
{
 
    // This var holds the leftmost 8
    // bits of the output.
 
    int leftmost_byte;
 
    // This holds the left middle
    // 8 bits of the output
 
    int left_middle_byle;
 
    // This holds the right middle
    // 8 bits of the output
 
    int right_middle_byte;
 
    // This holds the rightmost
    // 8 bits of the output
 
    int rightmost_byte;
 
    // To store the result
    // after conversion
 
    int result;
 
    // Get the rightmost 8 bits of the number
    // by anding it 0x000000FF. since the last
    // 8 bits are all ones, the result will be the
    // rightmost 8 bits of the number. this will
    // be converted into the leftmost 8 bits for the
    // output (swapping)
 
    leftmost_byte = (value & 0x000000FF) >> 0;
 
    // Similarly, get the right middle and left
    // middle 8 bits which will become
    // the left_middle bits in the output
 
    left_middle_byle = (value & 0x0000FF00) >> 8;
 
    right_middle_byte = (value & 0x00FF0000) >> 16;
 
    // Get the leftmost 8 bits which will be the
    // rightmost 8 bits of the output
 
    rightmost_byte = (value & 0xFF000000) >> 24;
 
    // Left shift the 8 bits by 24
    // so that it is shifted to the
    // leftmost end
 
    leftmost_byte <<= 24;
 
    // Similarly, left shift by 16
    // so that it is in the left_middle
    // position. i.e, it starts at the
    // 9th bit from the left and ends at the
    // 16th bit from the left
 
    left_middle_byle <<= 16;
 
    right_middle_byte <<= 8;
 
    // The rightmost bit stays as it is
    // as it is in the correct position
 
    rightmost_byte <<= 0;
 
    // Result is the concatenation of all these values.
 
    result = (leftmost_byte | left_middle_byle
              | right_middle_byte | rightmost_byte);
 
    return result;
}

/* implementation */
/* Encode a character in utf8 */
int utf8_encode(unsigned char *buf, uint32_t c) {
  if (c <= 0x7F) {
    buf[0] = c & 0b11111111;
    return 1;
  } else if (c <= 0x7FF) {
    buf[0] = 0b11000000 | (c >> 6 & 0b11111111);
    buf[1] = 0b10000000 | (c & 0b00111111);
    return 2;
  } else if (c <= 0xFFFF) {
    buf[0] = 0b11100000 | (c >> 12 & 0b11111111);
    buf[1] = 0b10000000 | (c >> 6 & 0b00111111);
    buf[2] = 0b10000000 | (c & 0b00111111);
    return 3;
  } else if (c <= 0x10FFFF) {
    buf[0] = 0b11110000 | (c >> 18 & 0b11111111);
    buf[1] = 0b10000000 | (c >> 12 & 0b00111111);
    buf[2] = 0b10000000 | (c >> 6 & 0b00111111);
    buf[3] = 0b10000000 | (c & 0b00111111);
    return 4;
  }

  return 0;
}

/* Decode an utf8 sequence to codepoint */
/* return codepoint and new position of utf8 sequence */
uint32_t utf8_decode(char *p, char **newp) {
  if (*p <= 0b01111111) {
    *newp = p + 1;
    return *p;
  }

  int len = 0;
  uint32_t c=0x0000;

  unsigned char byte1 = (unsigned char)*p;

  if (byte1 <= 0b11011111) {
    len = 2;
    c = 0b00011111 & byte1;
  } else if (byte1 <= 0b11101111) {
    len = 3;
    c = 0b00001111 & byte1;
  } else if (byte1 <= 0b11110111) {
    len = 4;
    c = 0b00000111 & byte1;
  } else {
    assert(0 && "invalid utf8 sequence");
  }

  /* if (byte1 >= 0b11110000){ */
  /*   len=4; */
  /*   c=byte1 & 0b111; */
  /* }else if (byte1 >= 0b11100000){ */
  /*   len=3; */
  /*   c = byte1 & 0b1111; */
  /* }else if (byte1 >= 0b11000000){ */
  /*   len=2; */
  /*   c= byte1 & 0b111111; */
  /* }else { */
  /*   assert(0 && "invalid utf8 sequence"); */
  /* } */
  
  for (int i = 1; i < len; i++) {
    unsigned char bytei = (unsigned char)p[i];
    if (bytei >> 6 != 0b10) assert(0 && "invalid utf8 sequence");
    c = (c << 6) | (bytei & 0b00111111);
  }

  *newp = p + len;
  /* return swap_endians(c); */
  return c;
}

int utf8_length(char *p) {
  if (*p <= 0b01111111) {
    return 1;
  }

  int len = 0;

  unsigned char byte1 = (unsigned char)*p;

  if (byte1 <= 0b11011111) {
    len = 2;
  } else if (byte1 <= 0b11101111) {
    len = 3;
  } else if (byte1 <= 0b11110111) {
    len = 4;
  }

  if (len < 1) {
    fprintf(stderr, "%s is invalid utf8 sequence.\n", p);
    return 0;
  }

  for (int i = 1; i < len; i++) {
    unsigned char bytei = p[i];
    if (bytei >> 6 != 0b010) {
      fprintf(stderr, "%s is invalid utf8 sequence.\n", p);
      return 0;
    }
  }

  return len;
}

bool utf8_is_legal(char *p){
  return utf8_length(p)>0;
}

#endif
